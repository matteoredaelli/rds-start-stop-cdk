#!/usr/bin/env python3

from aws_cdk import core

from rds_start_stop_cdk.rds_start_cdk_stack import RdsStartCdkStack

from rds_start_stop_cdk.rds_stop_cdk_stack import RdsStopCdkStack

app = core.App()
RdsStartCdkStack(app, "rds-start-cdk")
RdsStopCdkStack(app,  "rds-stop-cdk")

app.synth()
