# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import botocore
import boto3

from botocore.exceptions import ClientError

def lambda_handler(event, context):
    # TODO implement
    rds = boto3.client('rds')
    lambdaFunc = boto3.client('lambda')
    print 'Trying to get Environment variable'
    try:
        funcResponse = lambdaFunc.get_function_configuration(
            FunctionName='RDSInstanceStart'
        )
        #print (funcResponse)
        # RDSInstanceName = event["RDSInstanceName"]
        RDSInstanceName = funcResponse['Environment']['Variables']['RDSInstanceName']

        print 'Starting RDS service for RDSInstanceName : ' + RDSInstanceName
    except ClientError as e:
        print(e)
    try:
        response = rds.start_db_instance(
            DBInstanceIdentifier=RDSInstanceName
        )
        print 'Success :: '
        return response
    except ClientError as e:
        print(e)
    return
    {
        'message' : "Script execution completed. See Cloudwatch logs for complete output"
    }
